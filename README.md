# Martin Pišna - ZWA project
## Project description

The [project](http://wa.toad.cz/~pisnamar) is an educational fan-page for the computer game [Counter-Strike: Global Offensive](https://store.steampowered.com/app/730/CounterStrike_Global_Offensive/). The page displays an overview of an in-game map with buttons in different key positions. Selecting a position changes the output in a window next to the minimap based on which part of the map was selected. The window contains an embedded YouTube video showing how to throw a smoke grenade so that it always lands on the desired spot.

## Features

- **Interactive map:** clicking different places changes what is displayed on-screen WITHOUT reloading the page
- **Custom background color:** users can set any color as the background color, with automatic night mode (when a dark color is selected, the font changes from black to white and vice versa). Colors are saved locally for non-logged-in users and on the server for logged-in users.
- **Account creation:** visitors to the site can create accounts. Accounts are saved in a file on the server, passwords are hashed and salted. The website is secure against XSS attacks. Usernames and e-mails are unique.
- **Viewing users:** site visitors can see all the accounts names and their favorite colors, if they've selected one.

## Documentation
Please visit the [wiki](https://gitlab.com/mpisna2/martin-pisna-zwa-project/-/wikis/home) for the user manual and code documentation.
